"use strict";

/* 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

 let selectItem = document.createElement("select");

 selectItem.id = "rating";
//  selectItem.innerText = "We are choosing";

 console.log(selectItem);

 let maine = document.getElementById("maine");

 console.log(maine);

 maine.prepend(selectItem);

 let selectItem1 = document.createElement("option");
 let selectItem2 = document.createElement("option");
 let selectItem3 = document.createElement("option");
 let selectItem4 = document.createElement("option");

selectItem1.value = "1_Star";
selectItem1.innerText = "1 Star";
selectItem2.value = "2_Star";
selectItem2.innerText = "2 Stars";
selectItem3.value = "3_Star";
selectItem3.innerText = "3 Stars";
selectItem4.value = "4_Star";
selectItem4.innerText = "4 Stars";

 console.log(selectItem1);
 console.log(selectItem2);
 console.log(selectItem3);
 console.log(selectItem4);

 selectItem.append(selectItem1);
 selectItem.append(selectItem2);
 selectItem.append(selectItem3);
 selectItem.append(selectItem4);

 console.log(document.body);