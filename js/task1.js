"use strict";
/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". 
 Додайте цей елемент в footer після параграфу.
 
 */

 console.log("1. document.createElement(tagName):, document.createTextNode(text):, element.append(), append.child");
 console.log("2. node.remove(). пишемо імʼя ноди (контейнер nav). і прописуємо точку і слово remove. вона видалиться з DOM. поки ми її не повернемо або ще щось");
 console.log("3. append, prepend, before, after, replaceWith і insertAdjacentHTML/Text/Element з тими ж параметрами для вставки");

 console.log("PRACTICE TASKS");

 console.log("TASK 1");

 let footer = document.getElementById("futer");

 console.log(footer);

 let linkItem = document.createElement("a");

 linkItem.className = "newlink";
 linkItem.innerText = "Learn More";
 linkItem.href = "#";

 console.log(linkItem);

footer.append(linkItem);
// document.body.append(linkItem);

 console.log(document.body);
